---
title: "Superior Tribunal de Justicia"
subtitle: 
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: 
    pdf_document:
      toc: TRUE
      toc_depth: 3
      number_sections: TRUE
      includes:
        in_header: header.tex
        before_body: before_body.tex
geometry: margin=1.2cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
header-includes: \renewcommand{\contentsname}{Contenido del Documento}
params:
  circunscripcion:
    label: "Circunscripción (separar con coma sin espacio o 'Todas')"
    input: text
    value: "Todas"
  ieps: 
    label: "Organismos Judiciales (sd = sin discriminar)"
    input: text
    value: "sd"
  org_adm:
    label: "Organismo Administrativos (ej:APGE)"
    input: text
    value: ""
  fuero:
    label: "Fuero:Civil,Familia,Paz,Laboral,CAdm,ECQ,Penal,PEjec,PMediación,CMediación,Constitucional"
    input: text
    value: "Civil"
  instancia:
    label: "Instancia: 1, 2 o 3"
    input: text
    value: "1"
  start_date: 
    label: "Fecha_inicio_inf"
    input: text
    value: "2020-02-01"
  end_date:
    label: "Fecha_fin_inf"
    input: text
    value: "2020-06-01" 
  corte_temporal:
    label: "Corte Temporal"
    value: mensual
    input: select
    choices: [mensual, acumulado] 
  CausasIniciadas: FALSE 
  CausasArchivadas: FALSE 
  CausasResueltas: FALSE
  CausasEnTramite: FALSE
  CausasyMovimientos: FALSE
  ResolucionesxTipo: FALSE
  Audiencias: FALSE
  Duracion_Procesos: FALSE
  Pendientes: FALSE
  ConfirmaRevocaAnula: FALSE
  Personal: FALSE
  INDEC_Poblacion: FALSE
  _________________________Tabla: FALSE
  _________________________Tabla_Desagregada: FALSE
  _________________________Grafico: FALSE
  _________________________Primaria_Consolidada: FALSE
  Serie_Temporal:
    label: "Serie Temporal Agrupada (mes,org,circ)"
    input: text
    value: "sd" 
  AdHoc: FALSE
  AdHoc_tipo: 
    label: "Seleccione tipo de informe Ad-Hoc:"
    value: "sd"
    input: select
    choices: [penal_tproc_estupefacientes,paz23_ciniciadas_habitantes,
    civil_ejecuciones, paz_tvol, movimientos, violencias, indicadoresjus_cuarentena, grooming, medicos]
  Boletin_intro: FALSE
  Consideraciones_Finales: FALSE
  Consideraciones_Finales_codigo:
    label: "Consideraciones Finales: Código"
    input: numeric
    value: 0
  Metodología: FALSE
  Met_Presentaciones: FALSE
  Met_Observaciones: FALSE
  _________________________Seguimiento_Oralidad_Civil: FALSE
  _________________________Seguimiento_Paz_23: FALSE 
  _________________________Presentaciones_xOrganismo: FALSE 
---

```{r setup, include=FALSE}
informe_inicio <- Sys.time()
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
knitr::opts_chunk$set(fig.align='center') # fig.width=12, fig.height=14,
options(knitr.kable.NA = 'sd')
source("~/estudios-misc/R/informe.R")
```


```{r parametros, echo=FALSE, include=FALSE}
#-------------------------PARAMETROS:
# ------------------------parámetros de poblacion---------------------------
if (params$circunscripcion == "Todas"){
  circ <- NA
} else {
  circ <- unlist(str_split(params$circunscripcion, ","))
}

if (params$ieps == "sd"){
  ieps <- NA
} else {
  ieps <- unlist(str_split(params$ieps, ","))
}
fuero <- unlist(str_split(params$fuero, ",")) 
instancia <- unlist(str_split(params$instancia, ","))
instancia1 <- "1" %in% instancia
instancia2 <- "2" %in% instancia 
instancia3 <- "3" %in% instancia
org_adm <- unlist(str_split(params$org_adm, ","))
apge <- "APGE" %in% org_adm 
#-------------------------parametros temporales---------------------------
start_date <- params$start_date
end_date <- params$end_date
desagregacion_mensual <- params$corte_temporal == "mensual"
#-------------------------Indicadores--------------------------------------
ci <- params$CausasIniciadas
ca <- params$CausasArchivadas
cr <- params$CausasResueltas
s <- params$ResolucionesxTipo
ad <- params$Audiencias
ce <- params$CausasEnTramite
cm <- params$CausasyMovimientos
d <- params$Duracion_Procesos
pend <- params$Pendientes
cra <- params$ConfirmaRevocaAnula
p <- params$Personal
pob <- params$INDEC_Poblacion
#-------------------------parametros configuracion salida---------------------
t <- params$'_________________________Tabla'
td <- params$'_________________________Tabla_Desagregada'
g <- params$'_________________________Grafico'
stag <- unlist(str_split(params$Serie_Temporal, ","))
pc <- params$'_________________________Primaria_Consolidada'
#-------------------------parametros de metodología y documentacion------------
bo <- params$Boletin_intro
cf <- params$Consideraciones_Finales
cfc <- params$Consideraciones_Finales_codigo
metodologia <- params$Metodología
presentaciones <- params$Met_Presentaciones
observac <- params$Met_Observaciones
#-------------------------Seguimientos-----------------------------------------
oc <- params$'_________________________Seguimiento_Oralidad_Civil'
p23 <- params$'_________________________Seguimiento_Paz_23' 
pxo <- params$'_________________________Presentaciones_xOrganismo' 
#-----------------------------ADHOC--------------------------------------
ah <- params$AdHoc
aht <- unlist(str_split(params$AdHoc_tipo, ","))
```


```{r poblacion, echo=FALSE, include=FALSE}
# Poblacion---------------------------------------------------------------

# Entidades completo
poblacion_er <- apgyeJusEROrganization::listar_organismos() 

# Fuero Civil-Comercial y Laboral
poblacion_total <- poblacion_er %>% 
  filter(tipo %in% c("jdo", "cam", "sal", "cen")) 

if (exists("circ")) {
  if(is.na(circ)) {
    poblacion_total <- poblacion_total
  } else {
    poblacion_total <- poblacion_total %>%
      filter(circunscripcion %in% circ)
  }
}

if (exists("ieps")) {
    if(is.na(ieps)) {
    poblacion_total <- poblacion_total
    } else {
    poblacion_total <- poblacion_total %>%
      filter(organismo %in% ieps)
    poblacion_total
    }
}

# Sub-poblaciones
jdos_cco <- poblacion_total %>% 
  filter(grepl("jdocco|jdopen0000fel", organismo)) %>% 
  filter(str_detect(materia, "cco"), categoria == "1" | is.na(categoria)) 

if (any(str_detect(jdos_cco$materia, "lab|fam"))) { 
  multifueros <- TRUE 
  } else {
  multifueros <- FALSE
  }

jdos_ecq <- poblacion_total %>% 
  filter(grepl("jdocco", organismo)) %>% 
  filter(str_detect(materia, "eje|cqb"), categoria == "1" | is.na(categoria))

jdos_fam <- poblacion_total %>% 
  filter(grepl("fam", organismo)) %>% 
  filter(str_detect(materia, "fam"), tipo != "cam", categoria == "1" | is.na(categoria)) 

jdos_paz <- poblacion_total %>% 
  filter(str_detect(materia, "paz"), 
         categoria == "1"  
         | organismo == "jdopaz0000ram" 
         | organismo == "jdopaz0000cre" 
         | organismo == "jdopaz0000ove" 
         | organismo == "jdopaz0000sbe"
         | organismo == "jdopaz0000vpa"
         | organismo == "jdopaz0000vel"
         | organismo == "jdopaz0000sjo"
         | organismo == "jdopaz0000sel"
         | organismo == "jdopaz0000bov")

jdos_paz_23 <- poblacion_total %>% 
  filter(grepl("paz", organismo)) %>% 
  filter(str_detect(materia, "paz"), tipo != "cam", categoria != "1" | is.na(categoria))

if (nrow(jdos_paz_23) != 0) { 
  paz_23 <- TRUE 
  } else {
  paz_23 <- FALSE
  }

cen <- poblacion_total %>% 
  filter(grepl("cen", organismo))

jdos_lab <- poblacion_total %>% 
  filter(str_detect(organismo, "jdolab"), tipo != "cam")

cam_civil <- poblacion_total %>% 
  filter(str_detect(materia, "cco"), tipo == "cam")

cam_lab <- poblacion_total %>% 
  filter(str_detect(materia, "lab"), tipo == "cam")

sal_civil <- poblacion_total %>% 
  filter(str_detect(materia, "cco"), tipo == "sal")

sal_lab <- poblacion_total %>% 
  filter(str_detect(materia, "lab"), tipo == "sal")

# Fuero Penal

poblacion_penal <- poblacion_er %>% 
  filter(str_detect(fuero, "Penal")) 

if (exists("circ")) {
  if(is.na(circ)) {
    poblacion_penal <- poblacion_penal
  } else {
    poblacion_penal <- poblacion_penal %>%
      filter(circunscripcion %in% circ)
  }
}

if (exists("ieps")) {
    if(is.na(ieps)) {
    poblacion_penal <- poblacion_penal
    } else {
    poblacion_penal <- poblacion_penal %>%
      filter(organismo %in% ieps)
    poblacion_penal
    }
}

oga <- poblacion_penal %>% 
  filter(str_detect(organismo, "oga|jez"))

oma <- poblacion_penal %>% 
  filter(str_detect(organismo, "oma|equ"))

tja <- poblacion_penal %>% 
  filter(str_detect(organismo, "tja"))

jdo_pna <- poblacion_penal %>% 
  filter(str_detect(organismo, "jdopna"))

jdo_pep <- poblacion_penal %>% 
  filter(str_detect(organismo, "jdopep"))

cam_pen <- poblacion_penal %>% 
  filter(str_detect(organismo, "campen"), str_detect(materia, "pen"))

sal_pen <- poblacion_penal %>% 
  filter(str_detect(organismo, "sal"))

# Fuero Cont_Adm
cam_cad <- poblacion_er %>% 
  filter(str_detect(materia, "cad"), tipo == "cam") 

stj_cad <- poblacion_er %>% 
  filter(str_detect(materia, "cad"), tipo == "stj") 

# Fuero Constitucional
stj_pco <- poblacion_er %>% 
  filter(str_detect(materia, "pco"), tipo == "stj") 



```


\pagebreak

`r if (bo) '# Boletin Mensual de Estadística'`

```{r bo, child = 'boletin_intro.Rmd', eval = bo}
```

\pagebreak

`r if (instancia1 & "Civil" %in% fuero & (ci|cr|ad)) '# Juzgados Civil y Comercial'`


```{r cco1_cadur, child = 'cco1_cadur.Rmd', eval= instancia1 & "Civil" %in% fuero & d}
```

\pagebreak

```{r cco1_cetal, child = 'cco1_cetal.Rmd', eval= instancia1 & "Civil" %in% fuero & ce}
```

\pagebreak

```{r cco1_ci, child = 'cco1_ci.Rmd', eval= instancia1 & "Civil" %in% fuero & ci}
```

\pagebreak

```{r cco1_ci_multifueros, child = 'cco1_ci_multifueros.Rmd', eval = instancia1 & multifueros & "Civil" %in% fuero & ci & t}
```

\pagebreak

```{r cco1_cadr, child = 'cco1_cadr.Rmd', eval= instancia1 & "Civil" %in% fuero  & cr}
```

\pagebreak

```{r cco1_sentxtres, child = 'cco1_sentxtres.Rmd', eval= instancia1 & "Civil" %in% fuero & s}
```

\pagebreak

```{r cco1_audic_detalle, child = 'cco1_audic_detalle.Rmd', eval= instancia1 & "Civil" %in% fuero & ad}
```

\pagebreak

```{r cco1_camov, child = 'cco1_camov.Rmd', eval= instancia1 & "Civil" %in% fuero & cm}
```

\pagebreak
 
```{r cco1_carch, child = 'cco1_carch.Rmd', eval= instancia1 & "Civil" %in% fuero & ca}
```

\pagebreak

```{r cco1_pendientes, child = 'cco1_pendientes.Rmd', eval= instancia1 & "Civil" %in% fuero  & pend}
```

\pagebreak

```{r cco1_oralidad, child = 'cco1_oralidad_v2.Rmd', eval = instancia1 & "Civil" %in% fuero  & oc}
```

\pagebreak

`r if (instancia1 & "ECQ" %in% fuero) '# Juzgados Ejecuciones y Concursos-Quiebras'`

\pagebreak

```{r ecq1_cetal, child = 'ecq1_cetal.Rmd', eval= instancia1 & "ECQ" %in% fuero & ce}
```

\pagebreak

```{r ecq1_ci, child = 'ecq1_ci.Rmd', eval= instancia1 & "ECQ" %in% fuero & ci}
```

\pagebreak

```{r ecq1_cadr, child = 'ecq1_cadr.Rmd', eval= instancia1 & "ECQ" %in% fuero  & cr}
```

\pagebreak

```{r ecq1_sentxtres, child = 'ecq1_sentxtres.Rmd', eval= instancia1 & "ECQ" %in% fuero & s}
```

\pagebreak

```{r ecq1_camov, child = 'ecq1_camov.Rmd', eval= instancia1 & "ECQ" %in% fuero & cm}
```

\pagebreak
 
```{r ecq1_carch, child = 'ecq1_carch.Rmd', eval= instancia1 & "ECQ" %in% fuero & ca}
```

\pagebreak

```{r ecq1_pendientes, child = 'ecq1_pendientes.Rmd', eval= instancia1 & "ECQ" %in% fuero  & pend}
```

\pagebreak

```{r ecq1_durac, child = 'ecq1_durac.Rmd', eval= instancia1 & "ECQ" %in% fuero  & d}
```

 
`r if (instancia1 & "Familia" %in% fuero) '# Juzgados de Familia'`
 

```{r fam1_cadur, child = 'fam1_cadur.Rmd', eval= instancia1 & "Familia" %in% fuero & d}
```


\pagebreak

```{r fam1_cetal, child = 'fam1_cetal.Rmd', eval= instancia1 & "Familia" %in% fuero & ce }
```

\pagebreak

```{r fam1_ci, child = 'fam1_ci.Rmd', eval= instancia1 & "Familia" %in% fuero & ci }
```

\pagebreak

```{r fam1_cadr, child = 'fam1_cadr.Rmd', eval= instancia1 & "Familia" %in% fuero  & cr}
```

<!-- \pagebreak -->

<!-- ```{r fam1_sentxtres, child = 'fam1_sentxtres.Rmd', eval= instancia1 & "Familia" %in% fuero & s } -->
<!-- ``` -->

\pagebreak

```{r fam1_audif_detalle, child = 'fam1_audif_detalle.Rmd', eval= instancia1 & "Familia" %in% fuero & ad}
```

\pagebreak

```{r fam1_camov, child = 'fam1_camov.Rmd', eval= instancia1 & "Familia" %in% fuero & cm}
```

\pagebreak

 
```{r fam1_carch, child = 'fam1_carch.Rmd', eval= instancia1 & "Familia" %in% fuero & ca}
```

\pagebreak

```{r fam1_pendientes, child = 'fam1_pendientes.Rmd', eval= instancia1 & "Familia" %in% fuero  & pend}
```

\pagebreak

```{r violencias_adhoc, child = 'violencias_adhoc.Rmd', eval = ah & "violencias" %in% aht}
```

\pagebreak

`r if (instancia1 & "Paz" %in% fuero) '# Juzgados de Paz 1a. Categoría'`


```{r paz1_cetal, child = 'paz1_cetal.Rmd', eval= instancia1 & "Paz" %in% fuero & ce}
```

\pagebreak

```{r paz1_ci, child = 'paz1_ci.Rmd', eval= instancia1 & "Paz" %in% fuero & ci}
```

\pagebreak

```{r paz1_cadr, child = 'paz1_cadr.Rmd', eval= instancia1 & "Paz" %in% fuero  & cr}
```

\pagebreak

```{r paz1_audic_detalle, child = 'paz1_audic_detalle.Rmd', eval= instancia1 & "Paz" %in% fuero & ad}
```

\pagebreak

```{r paz1_camov, child = 'paz1_camov.Rmd', eval= instancia1 & "Paz" %in% fuero & cm}
```

\pagebreak

```{r paz1_carch, child = 'paz1_carch.Rmd', eval= instancia1 & "Paz" %in% fuero & ca}
```

\pagebreak

```{r paz1_pendientes, child = 'paz1_pendientes.Rmd', eval= instancia1 & "Paz" %in% fuero  & pend}
```

\pagebreak

`r if (instancia1 & "Paz" %in% fuero & paz_23 & ci) '# Juzgados Paz 2a. y 3a. Categoría'`


```{r paz23_ci, child = 'paz23_ci.Rmd', eval= instancia1 & "Paz" %in% fuero & ci & paz_23}
```

\pagebreak

```{r paz23_cadr, child = 'paz23_cadr.Rmd', eval= instancia1 & "Paz" %in% fuero & cr & paz_23 }
```

\pagebreak

```{r paz123_ci_graf, child = 'paz123_ci_graf.Rmd', eval= instancia1 & "Paz" %in% fuero & ci & g}
```

\pagebreak

```{r paz23_pendientes, child = 'paz23_pendientes.Rmd', eval= instancia1 & "Paz" %in% fuero & pend}
```

\pagebreak

```{r paz23_ci_adhoc, child = 'paz23_ci_adhoc.Rmd', eval= instancia1 & "Paz" %in% fuero & ah & ("paz23_ciniciadas_habitantes" %in% aht)}
```


\pagebreak

`r if (instancia1 & "Laboral" %in% fuero) '# Juzgados Laborales'`


```{r lab1_cadur, child = 'lab1_cadur.Rmd', eval= instancia1 & "Laboral" %in% fuero & d}
```

\pagebreak

```{r lab1_cetal, child = 'lab1_cetal.Rmd', eval= instancia1 & "Laboral" %in% fuero & ce}
```

\pagebreak

```{r lab1_ci, child = 'lab1_ci.Rmd', eval= instancia1 & "Laboral" %in% fuero & ci}
```

\pagebreak

```{r lab1_cadr, child = 'lab1_cadr.Rmd', eval= instancia1 & "Laboral" %in% fuero & cr}
```

\pagebreak

```{r lab1_sentxtres, child = 'lab1_sentxtres.Rmd', eval= instancia1 & "Laboral" %in% fuero & s}
```

\pagebreak

```{r lab1_audil_detalle, child = 'lab1_audil_detalle.Rmd', eval= instancia1 & "Laboral" %in% fuero & ad}
```

\pagebreak

```{r lab1_camov, child = 'lab1_camov.Rmd', eval= instancia1 & "Laboral" %in% fuero & cm}
```

\pagebreak

```{r lab1_carch, child = 'lab1_carch.Rmd', eval= instancia1 & "Laboral" %in% fuero & ca}
```

\pagebreak

```{r lab1_pendientes, child = 'lab1_pendientes.Rmd', eval= instancia1 & "Laboral" %in% fuero  & pend}
```

\pagebreak

`r if (instancia1 & "CMediación" %in% fuero) '# Centro De Medios Alternativos de Resolución de Conflictos - CEMARC'`

\pagebreak

```{r cen_ci, child = 'cen_ci.Rmd', eval= instancia1 & "CMediación" %in% fuero & ci}
```

\pagebreak

```{r cen_cr, child = 'cen_cr.Rmd', eval= instancia1 & "CMediación" %in% fuero & cr}
```

\pagebreak

```{r cen_mov, child = 'cen_mov.Rmd', eval= instancia1 & "CMediación" %in% fuero & cm}
```

\pagebreak

`r if (instancia1 & "Penal" %in% fuero) '# Garantías'`


```{r pen_gtia_ci, child = 'pen_gtia_ci.Rmd', eval= instancia1 & "Penal" %in% fuero & ci}
```

\pagebreak

```{r pen_gtia_res, child = 'pen_gtia_res.Rmd', eval= instancia1 & "Penal" %in% fuero & cr}
```

\pagebreak

```{r pen_gtia_audip, child = 'pen_gtia_audip.Rmd', eval= instancia1 & "Penal" %in% fuero & ad}
```

\pagebreak

```{r pen_gtia_adhoc, child = 'pen_gtia_adhoc.Rmd', eval= instancia1 & "Penal" %in% fuero & ah}
```


\pagebreak

`r if (instancia1 & "Penal" %in% fuero) '# Tribunal de Juicio'`


```{r pen_tja_ci, child = 'pen_tja_ci.Rmd', eval= instancia1 & "Penal" %in% fuero  & ci}
```

\pagebreak

```{r pen_tja_res, child = 'pen_tja_res.Rmd', eval= instancia1 & "Penal" %in% fuero & cr}
```

\pagebreak

```{r pen_tjui_audip, child = 'pen_tjui_audip.Rmd', eval= instancia1 & "Penal" %in% fuero & ad}
```

\pagebreak

```{r pen_tjui_adhoc, child = 'pen_tjui_adhoc.Rmd', eval= instancia1 & "Penal" %in% fuero & ah}
```

\pagebreak

`r if (instancia1 & "Penal" %in% fuero & (is.na(circ) | circ %in% c("Paraná")) & !("mes" %in% stag)) '# Penal de Menores - Niños y Adolescentes'`


```{r pen_pna_ci, child = 'pen_pna_ci.Rmd', eval= instancia1 & "Penal" %in% fuero & ci & (is.na(circ) | circ %in% c("Paraná")) & !("mes" %in% stag)}
```

\pagebreak

```{r pen_pna_res, child = 'pen_pna_res.Rmd', eval= instancia1 & "Penal" %in% fuero & cr & (is.na(circ) | circ %in% c("Paraná")) & !("mes" %in% stag)}
```

\pagebreak

```{r pen_pna_audip, child = 'pen_pna_audip.Rmd', eval= instancia1 & "Penal" %in% fuero & ad & (is.na(circ) | circ %in% c("Paraná")) & !("mes" %in% stag)}
```

\pagebreak

`r if (instancia1 & "PEjec" %in% fuero & (is.na(circ) | circ %in% c("Paraná", "Gualeguaychú"))) '# Ejecución de Penas y Medidas de Seguridad'`


```{r pen_pep_ci, child = 'pen_pep_ci.Rmd', eval= instancia1 & "PEjec" %in% fuero & (is.na(circ) | circ %in% c("Paraná", "Gualeguaychú")) & ci}
```

\pagebreak

```{r pen_pep_res, child = 'pen_pep_res.Rmd', eval= instancia1 & "PEjec" %in% fuero & (is.na(circ) | circ %in% c("Paraná", "Gualeguaychú")) & cr}
```

\pagebreak

`r if (instancia1 & "PMediación" %in% fuero & (ci | cr)) '# Oficina de Medios Alternativos - Penal'`

```{r pen_oma_ci, child = 'pen_oma_ci.Rmd', eval= instancia1 & "PMediación" %in% fuero & ci}
```

\pagebreak

```{r pen_oma_audip, child = 'pen_oma_audip.Rmd', eval= instancia1 & "PMediación" %in% fuero & ad}
```

\pagebreak

```{r pen_oma_camov, child = 'pen_oma_camov.Rmd', eval= instancia1 & "PMediación" %in% fuero & cm}
```


\pagebreak
 
`r if (instancia2 & "Civil" %in% fuero) '# Cámaras en lo Civil y Comercial'`
 

```{r cco2_ci, child = 'cco2_ci.Rmd', eval= instancia2 & "Civil" %in% fuero & ci }
```

\pagebreak

```{r cco2_cadr, child = 'cco2_cadr.Rmd', eval= instancia2 & "Civil" %in% fuero  & cr}
```


\pagebreak

```{r cco2_revoca_conf_anula, child = 'cco2_revoca_conf_anula.Rmd', eval= instancia2 & "Civil" %in% fuero  & cra }
```

\pagebreak

```{r cco2_cadur, child = 'cco2_cadur.Rmd', eval= instancia2 & "Civil" %in% fuero & d}
```

\pagebreak

```{r cco2_pendientes, child = 'cco2_pendientes.Rmd', eval= instancia2 & "Civil" %in% fuero  & pend}
```

\pagebreak
 
`r if (instancia2 & "Laboral" %in% fuero) '#Cámaras en lo Laboral'`
 

```{r lab2_ci, child = 'lab2_ci.Rmd', eval= instancia2 & "Laboral" %in% fuero & ci}
```

\pagebreak

```{r lab2_cadr, child = 'lab2_cadr.Rmd', eval= instancia2 & "Laboral" %in% fuero & cr }
```

\pagebreak

```{r lab2_revoca_conf_anula, child = 'lab2_revoca_conf_anula.Rmd', eval= instancia2 & "Laboral" %in% fuero  & cra }
```

\pagebreak

```{r lab2_pendientes, child = 'lab2_pendientes.Rmd', eval= instancia2 & "Laboral" %in% fuero  & pend}
```

\pagebreak

`r if (instancia1 & "CAdm" %in% fuero) '# Cámaras en lo Contencioso-Administrativo'`
 

```{r cad1_ci, child = 'cad1_ci.Rmd', eval= instancia1 & "CAdm" %in% fuero & ci}
```

\pagebreak

```{r cad1_cadr, child = 'cad1_cadr.Rmd', eval= instancia1 & "CAdm" %in% fuero  & cr}
```

\pagebreak

```{r cad1_resxtres, child = 'cad1_resxtres.Rmd', eval= instancia1 & "CAdm" %in% fuero  & s}
```

\pagebreak

```{r cad1_cadur, child = 'cad1_cadur.Rmd', eval= instancia1 & "CAdm" %in% fuero  & d}
```

\pagebreak

```{r cad1_cetal, child = 'cad1_cetal.Rmd', eval= instancia1 & "CAdm" %in% fuero & ce}
```


`r if (instancia2 & "Penal" %in% fuero) '# Cámaras de Casación'`
 

```{r pen_cam_ci, child = 'pen_cam_ci.Rmd', eval= instancia2 & "Penal" %in% fuero & ci}
```

\pagebreak

```{r pen_cam_res, child = 'pen_cam_res.Rmd', eval= instancia2 & "Penal" %in% fuero & cr}
```

\pagebreak

```{r pen_cam_audip, child = 'pen_cam_audip.Rmd', eval= instancia2 & "Penal" %in% fuero & ad}
```

\pagebreak

```{r pen_cam_resxtres, child = 'pen_cam_resxtres.Rmd', eval= instancia2 & "Penal" %in% fuero & s}
```

\pagebreak
 
`r if (instancia3 & "Civil" %in% fuero) '# Última Instancia Civil y Comercial'`
 
```{r cco3_ci, child = 'cco3_ci.Rmd', eval= instancia3 & "Civil" %in% fuero & ci }
```

\pagebreak

```{r cco3_cadr, child = 'cco3_cadr.Rmd', eval= instancia3 & "Civil" %in% fuero  & cr}
```

<!-- \pagebreak -->

<!-- ```{r cco3_tresxtorig, child = 'cco3_tresxtorig.Rmd', eval= instancia3 & "Civil" %in% fuero & cra} -->
<!-- ``` -->

\pagebreak

```{r cco3_sentxtres, child = 'cco3_sentxtres.Rmd', eval = instancia3 & "Civil" %in% fuero  & s}
```

\pagebreak

```{r cco3_pendientes, child = 'cco3_pendientes.Rmd', eval= instancia3 & "Civil" %in% fuero  & pend}
```

\pagebreak

`r if (instancia3 & "Laboral" %in% fuero) '# Última Instancia Laboral'`

```{r lab3_ci, child = 'lab3_ci.Rmd', eval= instancia3 & "Laboral" %in% fuero & ci }
```

\pagebreak

```{r lab3_cadr, child = 'lab3_cadr.Rmd', eval= instancia3 & "Laboral" %in% fuero  & cr}
```

\pagebreak

```{r lab3_sentxtres, child = 'lab3_sentxtres.Rmd', eval = instancia3 & "Civil" %in% fuero  & s}
```

\pagebreak

```{r lab3_pendientes, child = 'lab3_pendientes.Rmd', eval= instancia3 & "Laboral" %in% fuero  & pend}
```

\pagebreak

`r if (instancia3 & "Penal" %in% fuero) '# Última Instancia Penal'`

\pagebreak

```{r pen_sal_ci, child = 'pen_sal_ci.Rmd', eval = instancia3 & "Penal" %in% fuero & ci}
```

\pagebreak

```{r pen_sal_res, child = 'pen_sal_res.Rmd', eval = instancia3 & "Penal" %in% fuero & cr}
```

\pagebreak

`r if (instancia3 & "CAdm" %in% fuero) '# Última Instancia Contencioso Administrativo'`


```{r cad3_ci, child = 'cad3_ci.Rmd', eval= instancia3 & "CAdm" %in% fuero & ci}
```

\pagebreak

```{r cad3_cadr, child = 'cad3_cadr.Rmd', eval= instancia3 & "CAdm" %in% fuero  & cr}
```

\pagebreak

```{r cad3_sentxtres, child = 'cad3_sentxtres.Rmd', eval= instancia3 & "CAdm" %in% fuero & s}
```

\pagebreak

```{r cad3_revoca_conf, child = 'cad3_revoca_conf.Rmd', eval= instancia3 & "CAdm" %in% fuero & cra}
```

\pagebreak

```{r cad3_camov, child = 'cad3_camov.Rmd', eval= instancia3 & "CAdm" %in% fuero & cm}
```

\pagebreak

```{r cad3_cadur, child = 'cad3_cadur.Rmd', eval= instancia3 & "CAdm" %in% fuero & d}
```

\pagebreak

`r if (instancia3 & "Constitucional" %in% fuero) '# Última Instancia Constitucional'`


```{r const3_inic, child = 'const3_inic.Rmd', eval = instancia3 & "Constitucional" %in% fuero & ci}
```

\pagebreak

```{r const3_res, child = 'const3_res.Rmd', eval = instancia3 & "Constitucional" %in% fuero & cr}
```

\pagebreak

`r if (pxo) '# Informe de Presentaciones'`

```{r presentac_xorg, child = 'presentac_xorg.Rmd', eval = !is.na(ieps) & pxo}
```

\pagebreak

`r if (cf) '# Consideraciones Finales'`

```{r consideraciones_finales, child = 'consideraciones_finales.Rmd', eval= cf}
```

`r if (apge) '# Área de Planificación Gestión y Estadística'`

```{r apge_inic_resueltos, child = 'apge_inic_resueltos.Rmd', eval = apge}
```

\pagebreak

`r if (p) '# Personal'`

\pagebreak

```{r personal_cco, child = 'personal_cco.Rmd', eval = instancia1 & "Civil" %in% fuero & p}
```

\pagebreak

```{r personal_paz, child = 'personal_paz1.Rmd', eval = instancia1 & "Paz" %in% fuero & p}
```

\pagebreak

`r if (p) '# Movimientos Procesales'`

\pagebreak

```{r movimientos_adhoc, child = 'movimientos_adhoc.Rmd', eval = ah & "movimientos" %in% aht}
```

`r if (ah & "indicadoresjus_cuarentena" %in% aht) '# Indicadores judiciales y Cuarentena'`

\pagebreak

```{r pandemia_adhoc, child = 'pandemia_adhoc.Rmd', eval = ah & "indicadoresjus_cuarentena" %in% aht}
```


`r if (ah & "grooming" %in% aht) '# Delitos Informáticos: Grooming'`

\pagebreak

```{r gtia_grooming_adhoc, child = 'gtia_gooming_adhoc.Rmd', eval = ah & "grooming" %in% aht}
```


`r if (ah & "medicos" %in% aht) '# Análisis Exploratorio de Datos Departamento Médico'`

\pagebreak

```{r medicos_adhoc, child = 'medicos_adhoc.Rmd', eval = ah & "medicos" %in% aht}
```

\pagebreak

```{r metodologia, child = 'metodologia.Rmd', eval = metodologia}
```


***
```{r final}
tiempo <- as.character(round(lubridate::as.duration(x = Sys.time() - informe_inicio), 2))
```
\begin{center}
Superior Tribunal de Justicia de Entre Ríos - Área de Planificación Gestión y Estadística  
\end{center}
El presente informe se ha generado en: `r tiempo`

Director: Lic. Sebastián Castillo   
Equipo: Lic. Marcos Londero y Srta. Emilce Leones  
0343-4209405/410 – ints. 396 y 305  
http://www.jusentrerios.gov.ar/area-planificacion-y-estadisticas-stj/  
correos: apge@jusentrerios.gov.ar - estadistica@jusentrerios.gov.ar   
Laprida 250, Paraná, Entre Ríos  
